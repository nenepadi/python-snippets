import android
import time

myAndy = android.Android()
myAndy.toggleBluetoothState(True)
myAndy.dialogCreateAlert("Will be a server: ")
myAndy.dialogSetPositiveButtonText("Yes")
myAndy.dialogSetNegativeButtonText("No")
myAndy.dialogShow()

reply = myAndy.dialogGetResponse()
my_server = reply.result['which'] == 'positive'

if my_server:
    myAndy.bluetoothMakeDiscoverable()
    myAndy.bluetoothAccept()
else:
    myAndy.bluetoothConnect()
    
if my_server:
    reply = myAndy.dialogGetInput("Chat Me", "Enter your message here: ").result
    
    if reply is None:
        myAndy.exit()
    else:
        myAndy.bluetoothWrite(reply + '\n')
        