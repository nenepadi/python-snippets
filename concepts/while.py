def main():
    sum = 0
    counter = 0

    #a while loop to add numbers from 0 to 20
    while counter <= 20:
        sum += counter
        counter = counter + 1
    print(sum)
    
if __name__ == "__main__": main()
