#!/usr/bin/python3
#the main function    
def main():
    #testing a for loop
    a=0
    for i in range(10):
        a=a+1
        for j in range(10):
            a=a+1
    print(a)

    #testing a while loop
    done=False
    while not(done):
        quit = input ("Do you want to quit? ")
        if quit == "y" :
            done = True;
             
        attack = input ("Does your elf attack the dragon? ")
        if attack == "y":
            print ("Bad choice, you died.")
            done = True;

    #counting down to 10
    i = 10
    while i != 0:
        print (i)
        i -= 1
#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()
