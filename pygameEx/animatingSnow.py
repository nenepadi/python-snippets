#!/usr/bin/python3
"""
animatingSnow.py
Purpose: Testing animation in python with the pygame module
Maintainer: kizit2012@gmail.com
"""
#importing modules
import pygame
import random

#the main function
def main():
    #variables to handle colors...
    black = [ 0, 0, 0]
    white = [255,255,255]

    pygame.init()

    #setting the height and width of the screen
    size = [400,400]
    screen = pygame.display.set_mode(size)
    #setting the title of the window
    pygame.display.set_caption("Snow animation")

    #creating an empty list
    star_list = []

    #Loop about 50times and add a star in a random x, y position
    for k in range(50):
        x = random.randrange(0,400)
        y = random.randrange(0,400)
        star_list.append([x,y])

    # Used to manage how fast the screen updates
    clock=pygame.time.Clock()
    
    #Loop until the user clicks the close button.
    done=False
      
    # -------- Main Program Loop -----------
    while done==False:
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True # Flag that we are done so we exit this loop

        #setting the background
        screen.fill(black)
        
        # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
        
        for k in range(len(star_list)):
            #Drawing the star
            pygame.draw.circle(screen,white,star_list[k], 2)

            #moving star object down one pixel
            star_list[k][1] += 1

            #checking whether the star object does not bounce off the screen
            if star_list[k][1] > 400:
                #reset it by repositioning x and y
                y = random.randrange(-50,-10)
                star_list[k][1] = y
                x = random.randrange(0,400)
                star_list[k][0] = x
                
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
        #updating the screen with what we have drawn
        pygame.display.flip()
        # Limit to 20 frames per second
        clock.tick(20)

    #exiting the program
    pygame.quit()

#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()

    
