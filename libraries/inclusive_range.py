def inclusive_range(*args):
    ''' (list of ints)
    
    Behaves like range() but includes the number indicated as "stop".
    
    >>> for j in inclusive_range(10): print( j, end = ' ' ) 
    0 1 2 3 4 5 6 7 8 9 10
    '''

    number_args = len(args)
    if number_args < 1:
        raise TypeError( "This function requires at least one argument" )
    elif number_args == 1:
        stop = args[0]
        start = 0
        step = 1
    elif number_args == 2:
        ( start, stop ) = args
        step = 1
    elif number_args == 3:
        ( start, stop, step ) = args
    else:
        raise TypeError( "The function expects at most 3 arguments, u gave {}".format(number_args) )

    i = start
    while i <= stop:
        yield i
        i += step
        
#def main():
#    for j in inclusive_range(10):
#        print( j, end = ' ' )
    
#if __name__ == "__main__": main()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
