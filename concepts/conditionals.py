def main():
    a, b = 3, 3
    
    #this is an in-line conditional statement
    s = "a is less than b" if a < b else "a is not less than b"
    print(s)

    #a single conditional test statement with else
    if a > b:
        print("a is greater than b")
    else:
        print("a is not greater than b")

    #this is a multiple test performed with elif
    if a > b:
        print("a is greater than b")
    elif a < b:
        print("a is less than b")
    else:
        print("a is equal to b")

    
if __name__ == "__main__": main()
