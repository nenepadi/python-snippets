#!/usr/bin/python3
"""
graphicsTester.py
Purpose: Testing how graphics work in python
Maintainer: kizit2012@gmail.com
"""
#importing graphics handler
import pygame

#the main function
def main():
    #variables to handle colors...
    black = (0,0,0)
    white = (255,255,255)
    green = (0,255,0)
    red = (255,0,0)
    blue = (200,255, 0)

    pi=3.141592653

    pygame.init()

    #setting the height and width of the screen
    size = [700,650]
    screen = pygame.display.set_mode(size)
    #setting the title of the window
    pygame.display.set_caption("Nene Kizito's Library")

    #Loop until the user clicks the close button.
    done=False
      
    # Used to manage how fast the screen updates
    clock=pygame.time.Clock()
      
    # -------- Main Program Loop -----------
    while done==False:
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True # Flag that we are done so we exit this loop

        #setting the background
        screen.fill(white)
        # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
        #drawing a line
        pygame.draw.line(screen,green,[50,50],[100,100],5)

        #drawing so many lines with a loop and an offset
        y_offset=0
        while y_offset < 100:
            pygame.draw.line(screen,red,[0,10+y_offset],[100,110+y_offset],5)
            y_offset=y_offset+10

        #drawing a rectangle
        pygame.draw.rect(screen,green,[60,60,150,100],2)

        #drawing an ellipse
        pygame.draw.ellipse(screen,red,[200,200,200,100],2)

        # Draw an arc as part of an ellipse. Use radians to determine what
        # angle to draw.
        pygame.draw.arc(screen,green,[20,220,250,200], pi/2, pi, 2)
        pygame.draw.arc(screen,black,[20,220,250,200], 0, pi/2, 2)
        pygame.draw.arc(screen,red, [20,220,250,200],3*pi/2, 2*pi, 2)
        pygame.draw.arc(screen,blue, [20,220,250,200], pi,3*pi/2, 2)

        #drawing a polygon
        pygame.draw.polygon(screen,black,[[400,400],[200,500],[500,500],[600,450]],2)

        #experimenting with texts
        # Select the font to use. Default font, 25 pt size.
        font = pygame.font.Font(None, 25)
         
        # Render the text. "True" means anti-aliased text.
        # We create an image of the letters.
        text = font.render("Nene Padi Ademang",True,black)
         
        # Put the image of the text on the screen at 250x250
        screen.blit(text, [250,250])
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
          
        # Limit to 20 frames per second
        clock.tick(20)

        #updating the screen with what we have drawn
        pygame.display.flip()

    #exiting the program
    pygame.quit()

#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()

    
