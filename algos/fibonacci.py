#!/usr/bin/python3
#defining a function
def fibonacci(x):
    """(int) -> int

    Return the fibonacci series from fib(0) to fib(x)
    
    >>>fibonacci(10)
    1 1 2 3 5 8 13 21 34 55 89 
    """
    
    a, b = 0, 1
    for x in range(x+1):
        print(b, end = ' ')
        a, b = b, a+b
        
#the main function    
def main():
    #recieving input from the user...
    #...the input function allows only an input on a single line
    num = int(input("How many numbers will you like to display: "))

    #function call on fibonacci in main
    fibonacci(num)

#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()
