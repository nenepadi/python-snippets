def main():
    a, b = 0, 1

    #a simple fibonacci series
    while b <= 100:
        print(b, end=' ')
        a, b = b, a+b
    
if __name__ == "__main__": main()
