#!/usr/bin/python3
"""
advancedLoops.py
Purpose: Just to check my advancement with loops
Maintainer: kizit2012@gmail.com
"""

def main():
    print()
    for i in range(11):
        print("*", end = ' ')

    print()
    for j in range(11):
        print("*", end = ' ')
    for k in range(11):
        print("*", end)


if __name__ == "__main__": main()