#!/usr/bin/python3
"""
template.py
Purpose: Starting template for all of my works
Maintainer: kizit2012@gmail.com
"""
#importing graphics handler
import pygame

#the main function
def main():
    #variables to handle colors...
    black = (0,0,0)
    white = (255,255,255)

    #initialising the pygame module
    pygame.init()

    #setting the height and width of the screen
    size = (800, 600)
    screen = pygame.display.set_mode(size)
    #setting the title of the window
    pygame.display.set_caption("Template")

    #Loop until the user clicks the close button.
    done=False
      
    # Used to manage how fast the screen updates
    clock=pygame.time.Clock()
      
    # -------- Main Program Loop -----------
    while done==False:
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True # Flag that we are done so we exit this loop

        #setting the background
        screen.fill(white)
        # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
        
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
          
        # Limit to 20 frames per second
        clock.tick(20)

        #updating the screen with what we have drawn
        pygame.display.flip()

    #exiting the program
    pygame.quit()

#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()

    
