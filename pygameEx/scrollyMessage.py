#!/usr/bin/python3
"""
scrollyMessage.py
Purpose: show a message accross the screen with a background image
Maintainer: kizit2012@gmail.com
"""
#message = "HELLO BUDDY"

#importing graphics handler
import pygame

#the main function
def main():
    #variables to handle colors...
    black = (0,0,0)
    white = (255,255,255)

    #initialising the pygame module
    pygame.init()

    #setting the height and width of the screen
    size = (800, 600)
    screen = pygame.display.set_mode(size)
    
    #setting the title of the window
    pygame.display.set_caption("Scrolly Message")

    #creating a surface we can draw on...
    background = pygame.Surface(screen.get_size())

    #fill the screen background...
    background.fill(black)

    #variables to handle fonts...
    
    #other variables...
    #x = 0
    #y = (size[1] - text_surface.get_height()) / 2

    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()

    #set graphics position...
    background_position = (0,0)

    #background image handler...
    background_image_file = "myPix.jpg"
    background = pygame.image.load(background_image_file).convert()
    
    #Loop until the user clicks the close button.
    done=False
      
    # -------- Main Program Loop -----------
    while done==False:
        # Limit to 20 frames per second
        clock.tick(20)
        
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True # Flag that we are done so we exit this loop

        #copying image to screen
        screen.blit(background, background_position)
        # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT

        
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
          
        #updating the screen with what we have drawn
        pygame.display.flip()

    #exiting the program
    pygame.quit()

#this line tells the python interpreter to execute main()    
if __name__ == "__main__": main()

    
